import React, { Component } from 'react';
import './app.css'; 

class Form extends Component {
  constructor(props){
    super(props)
    this.state = {
      taskValue: '',
      tasksArray: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  
  render(){
    return (
        <div>
          <ul className="list-group" id="tasks">
            {this.state.tasksArray.map((tasks, index) => {
              return (
                <span key={index}>
                  <li className="list-group-item">{tasks.newTask}</li>{/* <i className="fas fa-trash-alt"></i> */}
                  <button type="button" className="btn btn-dark" onClick={this.deleteTask}>Del</button>
                </span>
              )
            })}
          </ul>
        </div>
      );
    }
  }
 

export default Form;
