import React, { Component } from 'react';
import './app.css';

import Form from "./form";

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="col-sm-6">
          <h1 className="title">ToDo List</h1>
          <Form />        
        </div>
      </div>
    );
  }
}

export default App;
