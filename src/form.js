import React, { Component } from 'react';
import './app.css'; 


class Form extends Component {
  constructor(props){
    super(props)
    this.state = {
      term: '',
      tasks: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event) => {
    this.setState({
      term: event.target.value
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.state.tasks.map((tasks, index) => {
        if(tasks == this.state.term || tasks == ""){
            alert("task already exists or is empty");
            return;
        }
    })
    this.setState({
        tasks: [...this.state.tasks, this.state.term]
    })
  }

  deleteTask = (event) => {
    var array = [...this.state.tasks]; // make a separate copy of the array
    var index = array.indexOf(event.target.value)
    array.splice(index, 1);
    this.setState({tasks: array});
  }

  render(){
    return (
        <div>
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label>Enter Task:</label>
                    <input type="text" className="form-control" value={this.term} onChange={this.handleChange} />
                </div>
                <button type="submit" className="btn btn-outline-dark">
                    Submit
                </button>
            </form>
            <br/>
            <ul className="list-group" id="tasks">
                {this.state.tasks.map((tasks, index) => {
                    return (
                        <li key={index} className="list-group-item">{tasks}><a onClick={this.deleteTask}><i class="fa fa-trash-o"></i></a></li>
                    )
                })}
            </ul>
        </div> 
      );
    }
  }
 

export default Form;
